#!/usr/bin/python
import os, sys
import hashlib
import requests
from requests.auth import HTTPBasicAuth
import argparse

parser = argparse.ArgumentParser(description='Upload artifact to Artifactory repository')

parser.add_argument('-t', '--target', required=True, help='Target file')
parser.add_argument('-a', '--url', required=True, help='Artifactory server Address')
parser.add_argument('-r', '--repo', required=True, help='Artifactory Repository Name')
parser.add_argument('-u', '--user', required=True, help='Artifactory Username')
parser.add_argument('-p', '--password', required=True, help='Artifactory Password')

args = parser.parse_args()

if not os.path.isfile(args.target):
    print args.target, ' File Not Found'
    sys.exit(2)

class Artifactory():

    def __init__(self, args={}):
        self.path= args.target
        self.url = args.url
        self.repo = args.repo
        self.username = args.user
        self.password = args.password

    def get_md5(self):
        md5 = hashlib.md5()
        with open(self.path, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), ''):
                md5.update(chunk)
        return md5.hexdigest()

    def get_sha1(self):
        sha1 = hashlib.sha1()
        with open(self.path, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), ''):
                sha1.update(chunk)
        return sha1.hexdigest()

    def upload(self, base_file_name = None):
        if base_file_name is None:
            base_file_name = os.path.basename(self.path)
        md5hash = self.get_md5()
        sha1hash = self.get_sha1()
        headers = {"X-Checksum-Md5": md5hash, "X-Checksum-Sha1": sha1hash}
        r = requests.put("{0}/{1}/{2}".format(self.url, self.repo, base_file_name),auth=(self.username,self.password), headers=headers, verify=False, data=open(self.path, 'rb'))
        return r.content

art = Artifactory(args)

print art.upload()

    # curl -uadmin:AP8iChb5v5XGEX2tYuvqe4Esg43 -T <PATH_TO_FILE> "http://art1.xrepeater.com/zip-local/<TARGET_FILE_PATH>"
